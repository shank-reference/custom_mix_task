defmodule Mix.Tasks.Scream do
  use Mix.Task

  @shortdoc "Scream the input"

  def run(args) do
    args
    |> Enum.join(" ")
    |> String.upcase
    |> Mix.shell.info
  end
end