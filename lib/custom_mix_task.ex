defmodule CustomMixTask do
  @moduledoc """
  Documentation for CustomMixTask.
  """

  @doc """
  Hello world.

  ## Examples

      iex> CustomMixTask.hello
      :world

  """
  def hello do
    :world
  end
end
